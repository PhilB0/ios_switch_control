/*********************************************************************
 This is an example for our nRF52 based Bluefruit LE modules

 Pick one up today in the adafruit shop!

 Adafruit invests time and resources providing this open source code,
 please support Adafruit and open-source hardware by purchasing
 products from Adafruit!

 MIT license, check LICENSE for more information
 All text above, and the splash screen below must be included in
 any redistribution
*********************************************************************/
#include <bluefruit.h>
//Phils
#define MY_DEBUG 1
#define IS_NRF 1

#define XRAY_SWITCH A0
#define CUTWIRE_SWITCH A1

#define EXIT_LIMIT 4000ul  //Time limit
int buttonStateS = 0;
int buttonStateC = 0;  
//bool timerGoingS = false; 
//bool pressedS = false;
//bool hitS = false;
//bool hitC = false;
//uint32_t startS=millis();

//EndPhils

bool hasKeyPressed = false;
BLEDis bledis;
BLEHidAdafruit blehid;



void setup() 
{

  //Phils
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(XRAY_SWITCH, INPUT_PULLUP);
  pinMode(CUTWIRE_SWITCH, INPUT_PULLUP);
  //EndPhils

  Serial.begin(115200);

  Serial.println("Bluefruit52 HID Keyboard Example");
  Serial.println("--------------------------------\n");

  Serial.println();
  Serial.println("Go to your phone's Bluetooth settings to pair your device");
  Serial.println("then open an application that accepts keyboard input");

  Serial.println();
  Serial.println("Enter the character(s) to send:");
  Serial.println();  

  Bluefruit.begin();
  // Set max power. Accepted values are: -40, -30, -20, -16, -12, -8, -4, 0, 4
  Bluefruit.setTxPower(4);
  Bluefruit.setName("FootPedal8");

  // Configure and Start Device Information Service
  bledis.setManufacturer("Adafruit Industries");
  bledis.setModel("Bluefruit Feather 52");
  bledis.begin();

  /* Start BLE HID
   * Note: Apple requires BLE device must have min connection interval >= 20m
   * ( The smaller the connection interval the faster we could send data).
   * However for HID and MIDI device, Apple could accept min connection interval 
   * up to 11.25 ms. Therefore BLEHidAdafruit::begin() will try to set the min and max
   * connection interval to 11.25  ms and 15 ms respectively for best performance.
   */
  blehid.begin();

  /* Set connection interval (min, max) to your perferred value.
   * Note: It is already set by BLEHidAdafruit::begin() to 11.25ms - 15ms
   * min = 9*1.25=11.25 ms, max = 12*1.25= 15 ms 
   */
  /* Bluefruit.setConnInterval(9, 12); */

  // Set up and start advertising
  startAdv();
}

void startAdv(void)
{  
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
  Bluefruit.Advertising.addAppearance(BLE_APPEARANCE_HID_KEYBOARD);
  
  // Include BLE HID service
  Bluefruit.Advertising.addService(blehid);

  // There is enough room for the dev name in the advertising packet
  Bluefruit.Advertising.addName();
  
  /* Start Advertising
   * - Enable auto advertising if disconnected
   * - Interval:  fast mode = 20 ms, slow mode = 152.5 ms
   * - Timeout for fast mode is 30 seconds
   * - Start(timeout) with timeout = 0 will advertise forever (until connected)
   * 
   * For recommended advertising interval
   * https://developer.apple.com/library/content/qa/qa1931/_index.html   
   */
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds
}



void loop() 
{
  

  // Only send KeyRelease if previously pressed to avoid sending
  // multiple keyRelease reports (that consume memory and bandwidth)
  if ( hasKeyPressed )
  {
    hasKeyPressed = false;
    blehid.keyRelease();
    // Delay a bit after a report
    delay(1000);
  }
 
  

//Phils
  //XRay
    buttonStateS = digitalRead(XRAY_SWITCH);
  //Serial.println( "---"+buttonStateS);

  // check if the pushbutton is pressed.
  // if it is, the buttonStateS is LOW:
  if (buttonStateS == LOW) {
    digitalWrite(LED_BUILTIN, HIGH);

    char myChar = 'S';
    Serial.println("Send a S");
    /* NRF version */
    if (IS_NRF) {/* NRF version */
      // echo
      Serial.println(myChar); 
      blehid.keyPress(myChar);
      delay (600);
    }
    hasKeyPressed = true;
    delay (600);
  } else {//Released
    // turn LED off:
    digitalWrite(LED_BUILTIN, LOW);
  //delay (50);
  }
  //CutWire
    buttonStateC = digitalRead(CUTWIRE_SWITCH);
  //Serial.println( "---"+buttonStateS);

  // check if the pushbutton is pressed.
  // if it is, the buttonStateS is LOW:
  if (buttonStateC == LOW) {
    digitalWrite(LED_BUILTIN, HIGH);

    char myChar = 'C';
    Serial.println("Send a C");
    /* NRF version */
    if (IS_NRF) {/* NRF version */
      // echo
      Serial.println(myChar); 
      blehid.keyPress(myChar);
      delay (600);
    }
    hasKeyPressed = true;
    delay (600);
  } else {//Released
    // turn LED off:
    digitalWrite(LED_BUILTIN, LOW);
  //delay (50);
  }



  //EndPhils

/*Old version */
  /*
 // read the state of the pushbutton value:
  buttonStateS = digitalRead(XRAY_SWITCH);
  Serial.println( '---'+StartPressS);
  // check if the pushbutton is pressed.
  // if it is, the buttonStateS is LOW:
  if (buttonStateS == LOW) {
    if (StartPressS) {Start=millis();StartPressS=false;}
    // turn LED off:
   // Serial.println('THis');
    digitalWrite(LED_BUILTIN, HIGH);
    if( (millis()-Start) > EXIT_LIMIT) {
      //Mode= OTHER_MODE;
      hasKeyPressedC = true;
    }
    else hasKeyPressedS = true;

  } else {//Released
    //Serial.println('That');
    // turn LED on:
    digitalWrite(LED_BUILTIN, LOW);
    //StartPressS=true;
    StartPressS = true;
  }

  

//Now Livescreen
  // read the state of the pushbutton value:
  buttonStateS = digitalRead(LIVES_SWITCH);

  // check if the pushbutton is pressed.
  // if it is, the buttonStateS is HIGH:
  if (buttonStateS == LOW) {
    if (StartPressC) {StartC=millis();StartPressC=false;}
    // turn LED off:
   // Serial.println('THis');
    digitalWrite(LED_BUILTIN, HIGH);
    if( (millis()-StartC) > EXIT_LIMIT) {
      //Mode= OTHER_MODE;
      hasKeyPressedC = true;
    }
    else hasKeyPressedS = true;

  } else {
    //Serial.println('That');
    // turn LED on:
    digitalWrite(LED_BUILTIN, LOW);
    StartPressC = true;
  }
if (hasKeyPressedC)
  {
    char ch = 'C';//(char) "C";//Serial.read();

    // echo
    Serial.println(ch); 

    blehid.keyPress(ch);
    hasKeyPressedC = false;
    hasKeyPressed = true;
    //StartPressS = true;
    
    // Delay a bit after a report
    delay(5000);
  }
  if (hasKeyPressedS)
  {
    char ch = 'S';//(char) "C";//Serial.read();

    // echo
    Serial.println(ch); 

    blehid.keyPress(ch);
    hasKeyPressedS = false;
    hasKeyPressed = true;
    
    // Delay a bit after a report
    delay(200);
  }
  if (hasKeyPressedS)
  {
    char ch = 'P';//(char) "C";//Serial.read();

    // echo
    Serial.println(ch); 

    blehid.keyPress(ch);
    hasKeyPressedS = false;
    hasKeyPressed = true;
    StartPressS = true;
    // Delay a bit after a report
    delay(200);
  }

*/



  // Request CPU to enter low-power mode until an event/interrupt occurs
  waitForEvent();  
}//EndOf NotGood

/**
 * RTOS Idle callback is automatically invoked by FreeRTOS
 * when there are no active threads. E.g when loop() calls delay() and
 * there is no bluetooth or hw event. This is the ideal place to handle
 * background data.
 * 
 * NOTE: It is recommended to call waitForEvent() to put MCU into low-power mode
 * at the end of this callback. You could also turn off other Peripherals such as
 * Serial/PWM and turn them back on if wanted
 *
 * e.g
 *
 * void rtos_idle_callback(void)
 * {
 *    Serial.stop(); // will lose data when sleeping
 *    waitForEvent();
 *    Serial.begin(115200);
 * }
 *
 * NOTE2: If rtos_idle_callback() is not defined at all. Bluefruit will force
 * waitForEvent() to save power. If you don't want MCU to sleep at all, define
 * an rtos_idle_callback() with empty body !
 * 
 * WARNING: This function MUST NOT call any blocking FreeRTOS API 
 * such as delay(), xSemaphoreTake() etc ... for more information
 * http://www.freertos.org/a00016.html
 */
void rtos_idle_callback(void)
{
  // Don't call any other FreeRTOS blocking API()
  // Perform background task(s) here

  // Request CPU to enter low-power mode until an event/interrupt occurs
  waitForEvent();
}
